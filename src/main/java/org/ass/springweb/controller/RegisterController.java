package org.ass.springweb.controller;

import org.ass.springweb.dto.RegisterDto;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RegisterController {
	
	RegisterController(){
		System.out.println("constructor call");
	}

	@RequestMapping(value= "/saveUser")
	public ModelAndView saveUser(RegisterDto registerDto){
		System.out.println(registerDto);
		return new ModelAndView("index.jsp");
	}
}
